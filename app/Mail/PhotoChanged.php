<?php

namespace App\Mail;

use app\Entities\Photo;
use App\Entities\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PhotoChanged extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $photo;

    public function __construct(User $user, Photo $photo)
    {
        $this->user = $user;
        $this->photo = $photo;
    }

    public function build()
    {
        return $this->view('email.photoChanged')
            ->with([
                'name' => $this->user->name,
                'photo_100_100' => $this->photo->photo100x100,
                'photo_150_150' => $this->photo->photo150x150,
                'photo_250_250' => $this->photo->photo250x250,
            ]);
    }
}
