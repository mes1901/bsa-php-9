<?php

namespace App\Http\Controllers;

use App\Entities\Photo;
use App\Jobs\CropJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PhotoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function upload(Request $request)
    {
        if (!$request->hasFile('photo')) {
            return new \Exception("something went wrong!");
        }
        $dir = 'images/' . Auth::id();
        if (!Storage::exists($dir)) {
            Storage::makeDirectory($dir);
        }
        $path = Storage::putFileAs(
            $dir,
            $request->file('photo'),
            $request->file('photo')->getClientOriginalName()
        );
        $photo = Photo::create([
            'user_id' => Auth::id(),
            'original_photo' => $path,
            'status' => 'uploaded'
        ]);
        CropJob::dispatch($photo);

        return response()->json($photo, 200);

    }
}
