<?php

namespace App\Notifications;

use App\Entities\Photo;
use App\Entities\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Storage;

class ImageProcessedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $photo;
    protected $user;

    public function __construct(Photo $photo, User $user)
    {
        $this->photo = $photo;
        $this->user = $user;
    }

    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line("Dear " . $this->user->name. ',')
            ->line("Photos have been successfully uploaded and processed.")
            ->line("Here are links to the images:")
            ->action('100_100', Storage::url($this->photo->photo_100_100))
            ->action('150_150', Storage::url($this->photo->photo_150_150))
            ->action('250_250', Storage::url($this->photo->photo_250_250))
            ->line("Thanks!");
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'status' => $this->photo->status,
            'photo_100_100' => Storage::url($this->photo->photo_100_100),
            'photo_150_150' => Storage::url($this->photo->photo_150_150),
            'photo_250_250' => Storage::url($this->photo->photo_250_250),
        ]);
    }

    public function toArray($notifiable)
    {
        return (array) $this->photo;
    }
}
