<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ImageProcessingFailedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $status;

    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public function via($notifiable)
    {
        return ['broadcast'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toBroadcast($notifiable){
        return new BroadcastMessage([
            'status' => $this->status,
        ]);
    }

    public function toArray($notifiable)
    {
        return [

        ];
    }
}
