<?php

namespace App\Jobs;

use App\Entities\Photo;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use App\Services\PhotoService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $photo;

    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    public function handle(PhotoService $photoService)
    {
        $this->photo->update([
            'photo_100_100' => $photoService->crop(
                $this->photo->original_photo, 100, 100
            ),
            'photo_150_150' => $photoService->crop(
                $this->photo->original_photo, 150, 150
            ),
            'photo_250_250' => $photoService->crop(
                $this->photo->original_photo, 250, 250
            ),
            'status' => 'success',
        ]);
        $user = Photo::find($this->photo->id)->user;
        $user->notify(new ImageProcessedNotification($this->photo, $user));
    }

    public function failed(\Exception $exception)
    {
        $this->photo->update([
            'status' => 'fail',
        ]);
        $user = Photo::find($this->photo->id)->user;
        $user->notify(new ImageProcessingFailedNotification($this->photo->status));
    }

}
