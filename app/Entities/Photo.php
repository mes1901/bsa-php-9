<?php

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'user_id',
        'original_photo',
        'photo_100_100',
        'photo_150_150',
        'photo_250_250',
        'status'
    ];

    protected $attributes = [
        'photo_100_100' => null,
        'photo_150_150' => null,
        'photo_250_250' => null,
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}